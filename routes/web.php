<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/', function () {
    return view('indeks');
});


Route::get('/mutabaah/pdf', 'MutabaahController@pdf')->middleware('auth');
Route::get('/mutabaah/grafik', 'MutabaahController@grafik')->middleware('auth');
Route::resource('mutabaah', 'MutabaahController')->middleware('auth');
Route::get('mutabaah/download/{type}', 'MutabaahController@download')->middleware('isadmin');

Route::resource('prodi', 'ProdiController')->middleware('isadmin');
Route::get('admin', 'DashboardAdminController@dashboard')->middleware('isadmin');
Route::resource('user', 'UserController')->middleware('isadmin');