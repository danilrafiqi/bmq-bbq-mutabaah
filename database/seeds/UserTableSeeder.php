<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
    	\App\User::insert([
			[
				'npm'				=> '123456789',
				'name'				=> 'Administrator',
				'email'				=> 'admin@bmq.com',
				'password'			=> '$2y$10$gLhBZUdKNRbJVGTDomRJxurE5RyHJvWPqXMxT8j.6jaN5/CHMxf8q',
				'sex'				=> 'male',
				'id_prodi'			=> '7530',
				'role'				=> 'admin',
				'foto'				=> 'user.png',
				'remember_token' 	=> '0cw0PriCEgNzfKwybIJFA1VgowlI97knVtnsVzqv',
				'created_at'		=> \Carbon\Carbon::now('Asia/Jakarta'),
				'updated_at'		=> \Carbon\Carbon::now('Asia/Jakarta'),
			]
    	]);
    }
}
