@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="info-box bg-pink hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">layers</i>
                    </div>
                    <div class="content">
                        <div class="text">Program Studi</div>
                        <div class="number count-to" data-from="0" data-to="{{$prodi}}" data-speed="15" data-fresh-interval="20">{{$prodi}}</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="info-box bg-cyan hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">account_circle</i>
                    </div>
                    <div class="content">
                        <div class="text">Ikhwan</div>
                        <div class="number count-to" data-from="0" data-to="257" data-speed="1000" data-fresh-interval="20">{{$ikhwan}}</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="info-box bg-light-green hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">face</i>
                    </div>
                    <div class="content">
                        <div class="text">Akhwat</div>
                        <div class="number count-to" data-from="0" data-to="243" data-speed="1000" data-fresh-interval="20">{{$akhwat}}</div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row clearfix">
          <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
              <div class="card">
                  <div class="header">
                      <h2>Mahasiswa Per Program Studi</h2>
                      <ul class="header-dropdown m-r--5">
                          <li class="dropdown">
                              <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                  <i class="material-icons">more_vert</i>
                              </a>
                              <ul class="dropdown-menu pull-right">
                                  <li><a href="javascript:void(0);">Action</a></li>
                                  <li><a href="javascript:void(0);">Another action</a></li>
                                  <li><a href="javascript:void(0);">Something else here</a></li>
                              </ul>
                          </li>
                      </ul>
                  </div>
                  <div class="body">
                      <canvas id="mhsprodi" style="height:400px"></canvas>
                  </div>
              </div>
          </div>

          <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
              <div class="card">
                  <div class="body bg-teal">
                      <div class="font-bold m-b--35">Recent Activity</div>
                      <ul class="dashboard-stat-list">
                          <li>
                              Minggu
                              <span class="pull-right"><b>NPM</b> <small><!-- Activity --></small></span>
                          </li>
                        @foreach($mutabaahs as $mutabaah)
                          <li>
                              {{$mutabaah['minggu']}}
                              <span class="pull-right"><b>{{$mutabaah['npm']}}</b> <small><!-- {{$mutabaah['created_at']}} --></small></span>
                          </li>
                        @endforeach
                      </ul>
                  </div>
              </div>
          </div>
        </div>
    </div>
<script src="{{ asset('js/Chart.bundle.js') }}"></script>
<script src="{{ asset('js/Chart.js') }}"></script>        

<script>
  var prodi =  <?php echo json_encode($namaprodi); ?>;;
  var jumlah =  {{$jumlah}};
  var ctx = document.getElementById("mhsprodi");
  var myChart = new Chart(ctx, {
      type: 'doughnut',
      data: {
          labels: prodi,
          datasets: [{
              label: 'Mahasiswa',
              backgroundColor:['#f48042','#65f441','#41f4dc', '#a341f4','#f44188','#f48042','#65f441','#41f4dc', '#a341f4','#f44188'],
              data: jumlah,  
              borderColor: 'rgba(54, 162, 235, 1)',
              borderWidth: 1
          }]
      },
        options: {
          responsive: true,
          legend: {
            position: 'left',
          },
          title: {
            display: true,
            text: 'Jumlah Mahasiswa Per PS'
          },
          animation: {
            animateScale: true,
            animateRotate: true
          }
        }
  });
</script>


@endsection