@extends('layouts.app')

@section('content')
        <div class="col-md-10">
            <div class="row">        
                <div class="col-md-4 col-sm-6 col-xs-12">
                  <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-university"></i></span>

                    <div class="info-box-content">
                      <span class="info-box-text">Program Studi</span>
                      <span class="info-box-number">{{$prodi}}</span>
                    </div>
                    <!-- /.info-box-content -->
                  </div>
                  <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <div class="col-md-4 col-sm-6 col-xs-12">
                  <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="fa fa-male"></i></span>

                    <div class="info-box-content">
                      <span class="info-box-text">Ikhwan</span>
                      <span class="info-box-number">41,410</span>
                    </div>
                    <!-- /.info-box-content -->
                  </div>
                  <!-- /.info-box -->
                </div>
                <!-- /.col -->

                <!-- fix for small devices only -->
                <div class="clearfix visible-sm-block"></div>

                <div class="col-md-4 col-sm-6 col-xs-12">
                  <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-female"></i></span>

                    <div class="info-box-content">
                      <span class="info-box-text">Akhwat</span>
                      <span class="info-box-number">760</span>
                    </div>
                    <!-- /.info-box-content -->
                  </div>
                  <!-- /.info-box -->
                </div>
            </div>

            <div class="row">
              <div class="col-md-8">
                <div class="box box-info">
                  <div class="box-header with-border">
                    <h3 class="box-title">Mahasiswa Program Studi</h3>
                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                  </div>
                  <div class="box-body">
                    <div class="chart">
                      <canvas id="mhsprodi" style="height:400px"></canvas>
                    </div>
                  </div>
                  <!-- /.box-body -->
                </div>
                
              </div>
              <div class="col-md-4">
                <div class="box box-info">
                  <div class="box-header with-border">
                    <h3 class="box-title">Update Terakhir Mahasiswa</h3>
                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                  </div>
                  <div class="box-body">
                    <div class="table-responsive">
                        <table class="table table-condensed table-bordered">
                            <thead>
                                <tr>
                                    <th>Minggu</th>
                                    <th>NPM</th>
                                    <th>Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                      @foreach($mutabaahs as $mutabaah)
                                      <tr>
                                        <td>{{$mutabaah['minggu']}}</td>
                                        <td>{{$mutabaah['npm']}}</td>
                                        <td>{{$mutabaah['created_at']}}</td>                                      
                                      </tr>
                                      @endforeach
                                </tr>
                            </tbody>
                        </table>
                    </div>
                  </div>
                  <!-- /.box-body -->
                </div>
                
              </div>

              <div class="col-md-12">
                <div class="box box-info">
                  <div class="box-header with-border">
                    <h3 class="box-title">Mahasiswa Program Studi</h3>
                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                  </div>
                  <div class="box-body">
                    <div class="chart">
                      <canvas id="mhsmingguan" style="height:300px"></canvas>
                    </div>
                  </div>
                  <!-- /.box-body -->
                </div>                
              </div>              
            </div>            
        </div>
<script src="{{ asset('js/Chart.bundle.js') }}"></script>
<script src="{{ asset('js/Chart.js') }}"></script>        

<script>
  var prodi =  <?php echo json_encode($namaprodi); ?>;;
  var jumlah =  {{$jumlah}};
  var ctx = document.getElementById("mhsprodi");
  var myChart = new Chart(ctx, {
      type: 'doughnut',
      data: {
          labels: prodi,
          datasets: [{
              label: 'Mahasiswa',
              backgroundColor:['#f48042','#65f441','#41f4dc', '#a341f4','#f44188','#f48042','#65f441','#41f4dc', '#a341f4','#f44188'],
              data: jumlah,  
              borderColor: 'rgba(54, 162, 235, 1)',
              borderWidth: 1
          }]
      },
        options: {
          responsive: true,
          legend: {
            position: 'left',
          },
          title: {
            display: true,
            text: 'Jumlah Mahasiswa Per PS'
          },
          animation: {
            animateScale: true,
            animateRotate: true
          }
        }
  });
</script>

<script type="text/javascript">  
  config = {
      type: 'bar',
      data: {
          labels: [
            "Sholat Wajib", 
            "Jamaah Awal Waktu", 
            "Subuh Berjamaah", 
            "Qiyamul Lail",
            "Witir", 
            "Istighfar",
            "Puasa Sunah", 
            "Tilawah Al Quran", 
            "Matsurat", 
            "Sholat Duha",
            "Baca Buku Islam", 
            "Riyadhoh",
            "Hafalan Quran",
            "Hafalan Hadist",],
          datasets: [
            <?php for($i=1; $i<=12; $i++){?>
            {
              label: "Minggu <?php echo $i;?>",
              data: [<?php echo rand(20,99).",".rand(20,99).",".rand(20,99).",".rand(20,99).",".rand(20,99).",".rand(20,99).",".rand(20,99).",".rand(20,99).",".rand(20,99).",".rand(20,99).",".rand(20,99).",".rand(20,99).",".rand(20,99).",".rand(20,99).",".rand(20,99).",".rand(20,99); ?>],
            backgroundColor:['#f48042','#65f441','#41f4dc', '#a341f4','#f44188','#f48042','#65f441','#41f4dc', '#a341f4','#f44188','#f48042','#65f441','#41f4dc', '#a341f4']
            }, 
            <?php }?>
          ]
      },
      options: {
          responsive: true,
          legend: false
      }
  }

  var ctx = document.getElementById("mhsmingguan");
  var myChart = new Chart(ctx,config);

</script>
@endsection