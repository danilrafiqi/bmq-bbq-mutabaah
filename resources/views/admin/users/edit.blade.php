@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header bg-green">
                        <h2>
                            User<small>Create Data User</small>
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);">Action</a></li>
                                    <li><a href="javascript:void(0);">Another action</a></li>
                                    <li><a href="javascript:void(0);">Something else here</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body">

		                <form method="POST" action="{{action('UserController@update', $id)}}" enctype="multipart/form-data">
		                    @csrf
                            <input name="_method" type="hidden" value="PATCH">                    	
		                    <div class="input-group">
		                        <span class="input-group-addon">
		                            <i class="material-icons">perm_identity</i>
		                        </span>
		                        <div class="form-line">
		                            <input id="npm" type="text" class="form-control{{ $errors->has('npm') ? ' is-invalid' : '' }}" name="npm" value="{{ $user->npm }}" placeholder="NPM/ID" required autofocus>
		                            @if ($errors->has('npm'))
		                                <span class="invalid-feedback">
		                                    <strong>{{ $errors->first('npm') }}</strong>
		                                </span>
		                            @endif
		                        </div>
		                    </div>

		                    <div class="input-group">
		                        <span class="input-group-addon">
		                            <i class="material-icons">dashboard</i>
		                        </span>
		                        <div class="form-line">
		                            <select class="form-control" id="sel1" class="form-control" name="id_prodi">
		                                @foreach($prodis as $prodi)
		                                	<option value="{{ $prodi['id'] }}" {{ $prodi['id'] == $user->id_prodi ? 'selected':'' }} >{{$prodi['nama_prodi']}}</option>
		                                @endforeach
		                            </select>

		                            @if ($errors->has('id_prodi'))
		                                <span class="invalid-feedback">
		                                    <strong>{{ $errors->first('id_prodi') }}</strong>
		                                </span>
		                            @endif		                            
		                        </div>
		                    </div>

		                    <div class="input-group">
		                        <span class="input-group-addon">
		                            <i class="material-icons">person</i>
		                        </span>
		                        <div class="form-line">
		                            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ $user->name }}" placeholder="Name" required>

		                            @if ($errors->has('name'))
		                                <span class="invalid-feedback">
		                                    <strong>{{ $errors->first('name') }}</strong>
		                                </span>
		                            @endif
		                        </div>
		                    </div>

		                    <div class="input-group">
		                        <span class="input-group-addon">
		                            <i class="material-icons">image</i>
		                        </span>
		                        <div class="form-line">
		                        	<div class="image">
			                        	<img src="{{asset('storage/uploads/'.$user->foto)}}" width="100px" height="100px" style="object-fit: cover;">
			                        </div>	
		                            <input id="foto" type="file" class="form-control{{ $errors->has('foto') ? ' is-invalid' : '' }}" name="foto" value="{{ $user->foto }}">

		                            @if ($errors->has('foto'))
		                                <span class="invalid-feedback">
		                                    <strong>{{ $errors->first('foto') }}</strong>
		                                </span>
		                            @endif
		                        </div>
		                    </div>



		                    <div class="input-group">
		                        <span class="input-group-addon">
		                            <i class="material-icons">email</i>
		                        </span>
		                        <div class="form-line">
		                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $user->email }}" placeholder="Email" required>

		                            @if ($errors->has('email'))
		                                <span class="invalid-feedback">
		                                    <strong>{{ $errors->first('email') }}</strong>
		                                </span>
		                            @endif                            
		                        </div>
		                    </div>

		                    <div class="input-group">
		                        <span class="input-group-addon">
		                            <i class="material-icons">account_circle</i>
		                        </span>
		                        <div class="form-line">
		                            <select class="form-control" id="sex" class="form-control" name="sex">
										<option value="male" {{ $user->sex=='male' ? 'selected':'' }} >Male</option>
										<option value="femal" {{ $user->sex=='female' ? 'selected':'' }} >Female</option>
		                            </select>
		                            @if ($errors->has('sex'))
		                                <span class="invalid-feedback">
		                                    <strong>{{ $errors->first('sex') }}</strong>
		                                </span>
		                            @endif
		                        </div>
		                    </div>


		                    <div class="input-group">
		                        <span class="input-group-addon">
		                            <i class="material-icons">dashboard</i>
		                        </span>
		                        <div class="form-line">
		                            <select class="form-control" id="sel1" class="form-control" name="role">
										<option value="user" {{ $user->role=='user' ? 'selected':'' }} >User</option>
										<option value="admin" {{ $user->role=='admin' ? 'selected':'' }} >Admin</option>
									</select>

		                            @if ($errors->has('role'))
		                                <span class="invalid-feedback">
		                                    <strong>{{ $errors->first('role') }}</strong>
		                                </span>
		                            @endif
		                        </div>
		                    </div>

		                    <button class="btn btn-lg btn-primary waves-effect" type="submit">{{ __('Update') }}</button>
		                </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

