@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                @if (\Session::has('success'))
                  <div class="alert alert-success">
                    <p>{{ \Session::get('success') }}</p>
                  </div><br />
                 @endif                    
                    <div class="header bg-green">
                        <h2>
                            User<small>Data User</small>
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <li>
                                <a href="{{action('UserController@create')}}">
                                    <i class="material-icons">add</i>
                                </a>
                            </li>
                          
                            <li>                                
                                <a href="javascript:void(0);" class="js-search" data-close="true">
                                    <i class="material-icons">search</i>
                                </a>                                
                            </li>
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);">Action</a></li>
                                    <li><a href="javascript:void(0);">Another action</a></li>
                                    <li><a href="javascript:void(0);">Something else here</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body">


                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                    <tr>
                                        <th>NPM/ID</th>
                                        <th>Nama </th>
                                        <th>Email</th>                                            
                                        <th>Hak Akses</th>
                                        <th colspan="2">Action</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>NPM/ID</th>
                                        <th>Nama </th>
                                        <th>Email</th>    
                                        <th>Hak Akses</th>
                                        <th colspan="2">Action</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    <tr>
                                        @foreach($users as $user)
                                        <tr>
                                            <td>{{$user['npm']}}</td>
                                            <td>{{$user['name']}}</td>
                                            <td>{{$user['email']}}</td>
                                            <td>{{$user['role']}}</td>
                                            
                                            <td class="js-sweetalert">
                                                <a href="{{action('UserController@edit', $user['npm'])}}" >
                                                    <button type="button" class="btn bg-green waves-effect btn-xs">
                                                        <i class="material-icons">edit</i>
                                                    </button>
                                                </a>

                                                <form action="{{action('UserController@destroy', $user['npm'])}}" method="post"  accept-charset="UTF-8" style="display:inline">
                                                    @csrf
                                                    <input name="_method" type="hidden" value="DELETE">
                                                    <button type="submit" class="btn bg-red waves-effect btn-xs" data-type="cancel" onclick="return confirm('Hapus ?')">
                                                        <i class="material-icons">delete</i>
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection