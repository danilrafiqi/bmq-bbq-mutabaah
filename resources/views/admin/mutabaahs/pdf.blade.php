<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Laporan Hasil BMQ</title>

            <style type="text/css">
                .page-break {
                    page-break-after: always;
                }
              table {
                border-collapse: collapse;
              }
              .table {
                width: 100%;
                max-width: 100%;
                margin-bottom: 1rem;
                background-color: transparent;
              }

              .table th,
              .table td {
                padding: 0.5rem;
                vertical-align: top;
                border-top: 1px solid #dee2e6;
              }

              .table thead th {
                vertical-align: bottom;
                border-bottom: 2px solid #dee2e6;
              }

              .table tbody + tbody {
                border-top: 2px solid #dee2e6;
              }

              .table .table {
                background-color: #fff;
              }

              .table-bordered {
                border: 1px solid #dee2e6;
              }

              .table-bordered th,
              .table-bordered td {
                border: 1px solid #dee2e6;
              }

              .table-bordered thead th,
              .table-bordered thead td {
                border-bottom-width: 2px;
              }

              /*Gambar*/
              td img{
                width: 120px;
              }
              td h1, td h2,td p,td a{                
                margin: 0;
                padding: 0;              
              }
              td h1{
                font-size: 140%;
              }
              td h2{
                font-size: 90%;
              }
              td p,td a{
                font-size: 80%;
              }
            </style>
	</head>
<body>
  <table width="100%">
    <tr>
      <td width="0%" align="center"><img src="{{base_path()}}/public/images/polinela.png" ></td>
      <td width="80%" align="center">
        <h2>KEMENTERIAN RISET TEKNOLOGI DAN PENDIDIKAN TINGGI</h2>
        <h1>POLITEKNIK NEGERI LAMPUNG</h1>
        <p>Jl. Soekarno-Hatta No.10, Rajabasa, Bandar Lampung, Lampung. Telp. 0721-703995</p>
        <a href="http://www.polinela.ac.id">http://www.polinela.ac.id</a>
      </td>
      <td width="0%" align="center"><img src="{{base_path()}}/public/images/albana.png" ></td>
    </tr>
    <tr><hr></tr>    
  </table>

  <table width="100%">
    <tr>
      <td align="center" width="100%"><h1>Laporan Mingguan BMQ</h1></td>
    </tr>
    <tr>
      <td align="center">Tahun <?php echo date('Y'); ?></td>
    </tr>
    <tr><hr></tr>
  </table>

  <br>
  <p>NPM {{$mahasiswa[0]->npm}}</p>
  <p>Nama {{$mahasiswa[0]->nama}}</p>
  <p>Program Studi {{$mahasiswa[0]->namaprodi}}</p>
  <hr>
  <br>

  <table class="table table-bordered" >
    
    <tr>
        <td>Minggu</td>
        @foreach($mutabaahs as $mutabaah)
            <td>{{$mutabaah['minggu']}}</td>
        @endforeach
    </tr>
    <tr>
        <td>Sholat Wajib</td>
        @foreach($mutabaahs as $mutabaah)
            <td>{{$mutabaah['sholat_wajib']}}</td>
        @endforeach
    </tr>
    <tr>
        <td>Sholat Berjamaah Awal Waktu</td>
        @foreach($mutabaahs as $mutabaah)
            <td>{{$mutabaah['jamaah_awal_waktu']}}</td>
        @endforeach
    </tr>
    <tr>
        <td>Sholat Subuh Berjamaah</td>
        @foreach($mutabaahs as $mutabaah)
            <td>{{$mutabaah['subuh_jamaah']}}</td>
        @endforeach
    </tr>
    <tr>
        <td>Qiyamul Lail</td>
        @foreach($mutabaahs as $mutabaah)            
            <td>{{$mutabaah['qiyamul_lail']}}</td>
        @endforeach
    </tr>
    <tr>
        <td>Witir</td>
        @foreach($mutabaahs as $mutabaah)
            <td>{{$mutabaah['witir']}}</td>
        @endforeach
    </tr>
    <tr>
        <td>Istighfar</td>
        @foreach($mutabaahs as $mutabaah)            
            <td>{{$mutabaah['istighfar']}}</td>
        @endforeach
    </tr>
    <tr>
        <td>Puasa Sunah</td>
        @foreach($mutabaahs as $mutabaah)            
            <td>{{$mutabaah['puasa_sunah']}}</td>
        @endforeach
    </tr>
    <tr>
        <td>Tilawah</td>     
        @foreach($mutabaahs as $mutabaah)            
            <td>{{$mutabaah['tilawah']}}</td>
        @endforeach
    </tr>
    <tr>
        <td>Matsurat</td>
        @foreach($mutabaahs as $mutabaah)            
            <td>{{$mutabaah['matsurat']}}</td>
        @endforeach
    </tr>
    <tr>
        <td>Sholat Duha</td>
        @foreach($mutabaahs as $mutabaah)            
            <td>{{$mutabaah['duha']}}</td>
        @endforeach
    </tr>
    <tr>
        <td>Baca Buku Islam</td>
        @foreach($mutabaahs as $mutabaah)            
            <td>{{$mutabaah['baca_buku_islam']}}</td>
        @endforeach
    </tr>
    <tr>
        <td>Hafalan Quran</td>  
        @foreach($mutabaahs as $mutabaah)            
            <td>{{$mutabaah['hafalan_quran']}}</td>
        @endforeach
    </tr>
    <tr>
        <td>Hafalan Hadist</td>    
        @foreach($mutabaahs as $mutabaah)            
            <td>{{$mutabaah['hafalan_hadis']}}</td>
        @endforeach
    </tr>
  </table>

</body>
</html>