<?php 

function randomHex() {
   $chars = 'ABCDEF0123456789';
   $color = '#';
   for ( $i = 0; $i < 6; $i++ ) {
      $color .= $chars[rand(0, strlen($chars) - 1)];
   }
   return $color;
}
?>

@extends('layouts.app')

@section('content')
  <div class="container-fluid">
    <div class="row clearfix">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
          <div class="header bg-green">
              <h2>
                  {{ __('Mutabaah Mingguan') }}
              </h2>
              <ul class="header-dropdown m-r--5">
                  <li class="dropdown">
                      <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                          <i class="material-icons">more_vert</i>
                      </a>
                      <ul class="dropdown-menu pull-right">
                          <li><a href="javascript:void(0);">Action</a></li>
                          <li><a href="javascript:void(0);">Another action</a></li>
                          <li><a href="javascript:void(0);">Something else here</a></li>
                      </ul>
                  </li>
              </ul>
          </div>
          <div class="body">
            <div class="chart">
              <canvas id="mhsmingguan" height="1300px" "></canvas>
            </div>            
          </div>
        </div>
      </div>
    </div>
  </div>

@include('layouts.includes.sliderbtn')

<script src="{{ asset('js/Chart.bundle.js') }}"></script>
<script src="{{ asset('js/Chart.js') }}"></script>        

<script type="text/javascript">  
  config = {
      type: 'horizontalBar',
      data: {
          labels: [
            "Sholat Wajib", 
            "Jamaah Awal Waktu", 
            "Subuh Berjamaah", 
            "Qiyamul Lail",
            "Witir", 
            "Istighfar",
            "Puasa Sunah", 
            "Tilawah Al Quran", 
            "Matsurat", 
            "Sholat Duha",
            "Baca Buku Islam", 
            "Riyadhoh",
            "Hafalan Quran",
            "Hafalan Hadist",],
          datasets: [

            @foreach($mutabaahs as $mutabaah)
            {
              label: "Minggu {{$mutabaah['minggu']}}",
              backgroundColor:<?php echo "'".randomHex()."'"; ?>,

              data: [
                {{$mutabaah['sholat_wajib']}},
                {{$mutabaah['jamaah_awal_waktu']}},
                {{$mutabaah['subuh_jamaah']}},
                {{$mutabaah['qiyamul_lail']}},
                {{$mutabaah['witir']}},
                {{$mutabaah['istighfar']}},
                {{$mutabaah['puasa_sunah']}},
                {{$mutabaah['tilawah']}},
                {{$mutabaah['matsurat']}},
                {{$mutabaah['duha']}},
                {{$mutabaah['baca_buku_islam']}},
                {{$mutabaah['hafalan_quran']}},
                {{$mutabaah['hafalan_hadis']}}
              ],
            }, 
            @endforeach
          ]
      },
      options: {
          responsive: true,
          legend: {
            position:'top'
          },
          scaleOptions: {
              yAxes: [{
                  display: true,
                  ticks: {
                      beginAtZero: true,
                      max: 100,
                      min: 0
                  }
              }]
          }
      }
  }

  var ctx = document.getElementById("mhsmingguan");
  var myChart = new Chart(ctx,config);

</script>
@endsection