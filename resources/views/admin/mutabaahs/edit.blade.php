@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header bg-green">
                        <h2>
                            {{ __('Mutabaah Mingguan') }}
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);">Action</a></li>
                                    <li><a href="javascript:void(0);">Another action</a></li>
                                    <li><a href="javascript:void(0);">Something else here</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <form method="post" action="{{action('MutabaahController@update', $id)}}">
                            @csrf
                            <input name="_method" type="hidden" value="PATCH">

                            <div class="row clearfix">
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                    <label for="minggu">{{ __('Minggu') }}</label>
                                </div>
                                <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input id="minggu" type="text" data-slider-min="0" data-slider-max="12" data-slider-step="1" data-slider-value="{{$mutabaah->minggu}}" name="minggu" />
                                            <span id="">Minggu ke : 
                                                <span id="minggu_val">{{$mutabaah->minggu}}</span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row clearfix">
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                    <label for="sholat_wajib">{{ __('Sholat Wajib') }}</label>
                                </div>
                                <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input id="sholat_wajib" type="text" data-slider-min="0" data-slider-max="35" data-slider-step="1" data-slider-value="{{$mutabaah->sholat_wajib}}" name="sholat_wajib" />
                                            <span id="">Jumlah : 
                                                <span id="sholat_wajib_val">{{$mutabaah->sholat_wajib}}</span> X
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>                            


                            <div class="row clearfix">
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                    <label for="jamaah_awal_waktu">{{ __('Sholat Berjamaah Awal Waktu') }}</label>
                                </div>
                                <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input id="jamaah_awal_waktu" type="text" data-slider-min="0" data-slider-max="35" data-slider-step="1" data-slider-value="{{$mutabaah->jamaah_awal_waktu}}" name="jamaah_awal_waktu" />
                                            <span id="">Jumlah : 
                                                <span id="jamaah_awal_waktu_val">{{$mutabaah->jamaah_awal_waktu}}</span> X
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row clearfix">
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                    <label for="subuh_jamaah">{{ __('Sholat Subuh Berjamaah') }}</label>
                                </div>
                                <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input id="subuh_jamaah" type="text" data-slider-min="0" data-slider-max="7" data-slider-step="1" data-slider-value="{{$mutabaah->subuh_jamaah}}" name="subuh_jamaah" />
                                            <span id="">Jumlah : 
                                                <span id="subuh_jamaah_val">{{$mutabaah->subuh_jamaah}}</span> X
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row clearfix">
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                    <label for="qiyamul_lail">{{ __('Qiyamul Lail') }}</label>
                                </div>
                                <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input id="qiyamul_lail" type="text" data-slider-min="0" data-slider-max="7" data-slider-step="1" data-slider-value="{{$mutabaah->qiyamul_lail}}" name="qiyamul_lail" />
                                            <span id="">Jumlah : 
                                                <span id="qiyamul_lail_val">{{$mutabaah->qiyamul_lail}}</span> X
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row clearfix">
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                    <label for="witir">{{ __('Sholat Witir') }}</label>
                                </div>
                                <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input id="witir" type="text" data-slider-min="0" data-slider-max="7" data-slider-step="1" data-slider-value="{{$mutabaah->witir}}" name="witir" />
                                            <span id="">Jumlah : 
                                                <span id="witir_val">{{$mutabaah->witir}}</span> X
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row clearfix">
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                    <label for="istighfar">{{ __('Istighfar') }}</label>
                                </div>
                                <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input id="istighfar" type="text" data-slider-min="0" data-slider-max="700" data-slider-step="1" data-slider-value="{{$mutabaah->istighfar}}" name="istighfar" />
                                            <span id="">Jumlah : 
                                                <span id="istighfar_val">{{$mutabaah->istighfar}}</span> X
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="row clearfix">
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                    <label for="puasa_sunah">{{ __('Puasa Sunah') }}</label>
                                </div>
                                <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input id="puasa_sunah" type="text" data-slider-min="0" data-slider-max="7" data-slider-step="1" data-slider-value="{{$mutabaah->puasa_sunah}}" name="puasa_sunah" />
                                            <span id="">Jumlah : 
                                                <span id="puasa_sunah_val">{{$mutabaah->puasa_sunah}}</span> X
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="row clearfix">
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                    <label for="tilawah">{{ __('Tilawah') }}</label>
                                </div>
                                <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input id="tilawah" type="text" data-slider-min="0" data-slider-max="70" data-slider-step="1" data-slider-value="{{$mutabaah->tilawah}}" name="tilawah" />
                                            <span id="">Jumlah : 
                                                <span id="tilawah_val">{{$mutabaah->tilawah}}</span> Halaman
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row clearfix">
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                    <label for="matsurat">{{ __('Matsurat') }}</label>
                                </div>
                                <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input id="matsurat" type="text" data-slider-min="0" data-slider-max="7" data-slider-step="1" data-slider-value="{{$mutabaah->matsurat}}" name="matsurat" />
                                            <span id="">Jumlah : 
                                                <span id="matsurat_val">{{$mutabaah->matsurat}}</span> X
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row clearfix">
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                    <label for="duha">{{ __('Sholat Dhuha') }}</label>
                                </div>
                                <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input id="duha" type="text" data-slider-min="0" data-slider-max="7" data-slider-step="1" data-slider-value="{{$mutabaah->duha}}" name="duha" />
                                            <span id="">Jumlah : 
                                                <span id="duha_val">{{$mutabaah->duha}}</span> X
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row clearfix">
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                    <label for="baca_buku_islam">{{ __('Baca Buku Islam') }}</label>
                                </div>
                                <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input id="baca_buku_islam" type="text" data-slider-min="0" data-slider-max="70" data-slider-step="1" data-slider-value="{{$mutabaah->baca_buku_islam}}" name="baca_buku_islam" />
                                            <span id="">Jumlah : 
                                                <span id="baca_buku_islam_val">{{$mutabaah->baca_buku_islam}}</span> Halaman
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row clearfix">
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                    <label for="hafalan_quran">{{ __('Hafalan Quran') }}</label>
                                </div>
                                <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input id="hafalan_quran" type="text" data-slider-min="0" data-slider-max="70" data-slider-step="1" data-slider-value="{{$mutabaah->hafalan_quran}}" name="hafalan_quran" />
                                            <span id="">Jumlah : 
                                                <span id="hafalan_quran_val">{{$mutabaah->hafalan_quran}}</span> Ayat
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row clearfix">
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                    <label for="hafalan_hadis">{{ __('Hafalan Hadist') }}</label>
                                </div>
                                <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input id="hafalan_hadis" type="text" data-slider-min="0" data-slider-max="70" data-slider-step="1" data-slider-value="{{$mutabaah->hafalan_hadis}}" name="hafalan_hadis" />
                                            <span id="">Jumlah : 
                                                <span id="hafalan_hadis_val">{{$mutabaah->hafalan_hadis}}</span> Ayat
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row clearfix">
                                <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                                    <button type="submit" class="btn btn-primary m-t-15 waves-effect">Create</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('layouts.includes.sliderbtn')
@endsection

