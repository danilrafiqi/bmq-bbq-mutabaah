@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                @if (\Session::has('success'))
                  <div class="alert alert-success">
                    <p>{{ \Session::get('success') }}</p>
                  </div><br />
                 @endif                    
                    <div class="header bg-green">
                        <h2>
                            Mutabaah<small>Data Mutabaah Mingguan</small>
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <li>
                                <a href="{{action('MutabaahController@create')}}">
                                    <i class="material-icons">add</i>
                                </a>
                            </li>
                          
                            <li>                                
                                <a href="javascript:void(0);" class="js-search" data-close="true">
                                    <i class="material-icons">search</i>
                                </a>                                
                            </li>
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    @if (Auth::user()->role == 'admin')
                                        <li>
                                            <a href="{{action('MutabaahController@download', 'csv')}}" >CSV</a>
                                        </li>
                                        <li>
                                            <a href="{{action('MutabaahController@download', 'xls')}}" >XLS</a>       
                                        </li>
                                    @else
                                        <li>
                                            <a href="{{action('MutabaahController@pdf')}}" >Export PDF</a>
                                        </li>
                                        <li>
                                            <a href="{{action('MutabaahController@grafik')}}" >Cek Grafik</a>       
                                        </li>
                                    @endif                                    
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                    <tr>
                                        @if (Auth::user()->role == 'admin')
                                            <th>No</th>
                                        @endif
                                        <th>Minggu</th>
                                        <th>NPM</th>
                                        <th>Sholat Wajib</th>
                                        <th>Sholat Berjamaah Awal Waktu</th>
                                        <th>Sholat Subuh Berjamaah</th>
                                        <th>Qiyamul Lail</th>
                                        <th>Witir</th>
                                        <th>Istighfar</th>
                                        <th>Puasa Sunah</th>
                                        <th>Tilawah</th>
                                        <th>Matsurat</th>
                                        <th>Sholat Duha</th>
                                        <th>Baca Buku Islam</th>
                                        <th>Hafalan Quran</th>
                                        <th>Hafalan Hadist</th>
                                        <th colspan="2">Action</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        @if (Auth::user()->role == 'admin')
                                            <th>No</th>
                                        @endif
                                        <th>Minggu</th>
                                        <th>NPM</th>
                                        <th>Sholat Wajib</th>
                                        <th>Sholat Berjamaah Awal Waktu</th>
                                        <th>Sholat Subuh Berjamaah</th>
                                        <th>Qiyamul Lail</th>
                                        <th>Witir</th>
                                        <th>Istighfar</th>
                                        <th>Puasa Sunah</th>
                                        <th>Tilawah</th>
                                        <th>Matsurat</th>
                                        <th>Sholat Duha</th>
                                        <th>Baca Buku Islam</th>
                                        <th>Hafalan Quran</th>
                                        <th>Hafalan Hadist</th>                                        
                                        <th colspan="2">Action</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    @foreach($mutabaahs as $mutabaah)
                                    <tr>
                                        @if (Auth::user()->role == 'admin')
                                            <td>{{$mutabaah['id']}}</td>
                                        @endif
                                        <td>{{$mutabaah['minggu']}}</td>
                                        <td>{{$mutabaah['npm']}}</td>
                                        <td>{{$mutabaah['sholat_wajib']}}</td>
                                        <td>{{$mutabaah['jamaah_awal_waktu']}}</td>
                                        <td>{{$mutabaah['subuh_jamaah']}}</td>
                                        <td>{{$mutabaah['qiyamul_lail']}}</td>
                                        <td>{{$mutabaah['witir']}}</td>
                                        <td>{{$mutabaah['istighfar']}}</td>
                                        <td>{{$mutabaah['puasa_sunah']}}</td>
                                        <td>{{$mutabaah['tilawah']}}</td>
                                        <td>{{$mutabaah['matsurat']}}</td>
                                        <td>{{$mutabaah['duha']}}</td>
                                        <td>{{$mutabaah['baca_buku_islam']}}</td>
                                        <td>{{$mutabaah['hafalan_quran']}}</td>
                                        <td>{{$mutabaah['hafalan_hadis']}}</td>

                                        <td align="right">
                                            <a href="{{action('MutabaahController@edit', $mutabaah['id'])}}" >
                                                <button type="button" class="btn bg-green waves-effect btn-xs">
                                                    <i class="material-icons">edit</i>
                                                </button>
                                            </a>
                                        </td>
                                        <td align="left">
                                            <form action="{{action('MutabaahController@destroy', $mutabaah['id'])}}" method="post"  accept-charset="UTF-8" style="display:inline" >
                                                @csrf
                                                <input name="_method" type="hidden" value="DELETE">
                                                <button type="submit" class="btn bg-red waves-effect btn-xs" data-type="cancel" onclick="return confirm('Hapus ?')">
                                                    <i class="material-icons">delete</i>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {!! $mutabaahs->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection