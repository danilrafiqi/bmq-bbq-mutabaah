        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <img src="{{asset('storage/uploads/'.Auth::user()->foto)}}" width="48" height="48" alt="User" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }}</div>
                    <div class="email">{{ Auth::user()->email }}</div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    <i class="material-icons">input</i>{{ __('Logout') }}
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">MAIN NAVIGATION</li>
                    @if (Auth::user()->role == 'admin')
                        <li {{ Request::is('admin*') ? 'class=active':'' }}>
                            <a href="/admin">
                                <i class="material-icons">insert_chart</i>
                                <span>Dashboard</span>
                            </a>
                        </li>
                        <li {{ Request::is('mutabaah*') ? 'class=active':'' }}>
                            <a href="/mutabaah">
                                <i class="material-icons">dashboard</i>
                                <span>Mutabaah</span>
                            </a>
                        </li>
                        <li {{ Request::is('prodi*') ? 'class=active':'' }}>
                            <a href="/prodi">
                                <i class="material-icons">layers</i>
                                <span>Program Studi</span>
                            </a>
                        </li>
                        <li {{ Request::is('user*') ? 'class=active':'' }}>
                            <a href="/user">
                                <i class="material-icons">account_circle</i>
                                <span>User</span>
                            </a>
                        </li>                            
                    @else
                        <li class="active">
                            <a href="/mutabaah">
                                <i class="material-icons">dashboard</i>
                                <span>Dashboard</span>
                            </a>
                        </li>
                    @endif
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 
                    <script type="text/javascript">
                        document.write(new Date().getFullYear());    
                    </script>
                    <a href="/">Mutabaah</a>.
                </div>
                <div class="version">
                    <b>Version: </b> 1.0
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->