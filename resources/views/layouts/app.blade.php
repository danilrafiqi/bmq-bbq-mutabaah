<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
  @include('layouts.head')
</head>

<body class="theme-green">
    @include('layouts.preloader')
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <form class="search-bar" action="{{ url()->current() }}" accept-charset="UTF-8" role="search">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING..." name="keyword" >
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </form>
    <!-- #END# Search Bar -->
    @include('layouts.topbar')
    <section>
        @include('layouts.leftsidebar')
    </section>

    <section class="content">
        @yield('content')
    </section>

    @include('layouts.script')
</body>

</html>