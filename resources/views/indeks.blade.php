<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <title>BMQ - Mutabaah Yaumiah</title>
  <link rel="icon" href="{{ asset('images/albana.png') }}" type="image/x-icon">

  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="{{asset('plugins/materialize-css/css/materialize-v1.css')}}" type="text/css" rel="stylesheet" media="screen,projection"/>
  <style type="text/css">
/* Custom Stylesheet */
/**
 * Use this file to override Materialize files so you can update
 * the core Materialize files in the future
 *
 * Made By MaterializeCSS.com
 */

nav ul a,
nav .brand-logo {
  color: #444;
}

p {
  line-height: 2rem;
}

.sidenav-trigger {
  color: #26a69a;
}

.parallax-container {
  min-height: 380px;
  line-height: 0;
  height: auto;
  color: rgba(255,255,255,.9);
}
  .parallax-container .section {
    width: 100%;
  }

@media only screen and (max-width : 992px) {
  .parallax-container .section {
    position: absolute;
    top: 40%;
  }
  #index-banner .section {
    top: 10%;
  }
}

@media only screen and (max-width : 600px) {
  #index-banner .section {
    top: 0;
  }
}

.icon-block {
  padding: 0 15px;
}
.icon-block .material-icons {
  font-size: inherit;
}

footer.page-footer {
  margin: 0;
}
      

  </style>
</head>
<body>
  <nav class="white" role="navigation">
    <div class="nav-wrapper container">
      <a id="logo-container" href="/" class="brand-logo">BMQ</a>

    @guest
      <ul class="right hide-on-med-and-down">
        <li><a href="{{ route('login') }}">Login</a></li>
      </ul>
      <ul id="nav-mobile" class="sidenav">
        <li><a href="{{ route('login') }}">Login</a></li>
      </ul>
    @else
      <ul class="right hide-on-med-and-down">
        <li>
            <a href="{{ route('logout') }}"
               onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                {{ __('Logout') }}
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </li>
      </ul>
      <ul id="nav-mobile" class="sidenav">
        <li>
            <a href="{{ route('logout') }}"
               onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                {{ __('Logout') }}
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </li>
      </ul>
    @endguest
      <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
    </div>
  </nav>

  <div id="index-banner" class="parallax-container">
    <div class="section no-pad-bot">
      <div class="container">
        <br><br>
        <h1 class="header center teal-text text-lighten-2">Mutabaah Yaumiah</h1>
        <div class="row center">
          <h5 class="header col s12 light">Monitoring Aktivitas ibadah anda dengan mudah</h5>
        </div>
        <div class="row center">
          <a href="/login" id="download-button" class="btn-large waves-effect waves-light teal lighten-1">Mulai</a>
        </div>
        <br><br>

      </div>
    </div>
    <div class="parallax"><img src="{{asset('images/background1.jpg')}}" alt="Unsplashed background img 1"></div>
  </div>


  <div class="no-container">
    <div class="section">

      <!--   Icon Section   -->
      <div class="row">
        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center brown-text"><i class="material-icons">layers</i></h2>
            <h5 class="center">Mudah Digunakan</h5>

            <p class="light">Aplikasi dibuat dengan memperhatikan kemudahan user dalam menggunakan, agar aplikasi menjadi maksimal.</p>
          </div>
        </div>

        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center brown-text"><i class="material-icons">insert_chart</i></h2>
            <h5 class="center">Monitoring</h5>

            <p class="light">Aplikasi sangat cocok digunakan untuk memonitoring diri sendiri dalam beribadah</p>
          </div>
        </div>

        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center brown-text"><i class="material-icons">dashboard</i></h2>
            <h5 class="center">Muhasabah</h5>

            <p class="light">Aplikasi dapat digunakan untuk muhasabah diri karena kita melihat perkembangan ibadah yang dilakukan</p>
          </div>
        </div>
      </div>

    </div>
  </div>


  <div class="parallax-container valign-wrapper">
    <div class="section no-pad-bot">
      <div class="container">
        <div class="row center">
          <h5 class="header col s12 light">Barang siapa berhijrah di jalan Allah, niscaya mereka akan mendapati di muka bumi ini tempat hijrah yang luas dan rezeki yang banyak</h5>
        </div>
      </div>
    </div>
    <div class="parallax"><img src="{{asset('images/background2.jpg')}}" alt="Unsplashed background img 2"></div>
  </div>

  <div class="container">
    <div class="section">

      <div class="row">
        <div class="col s12 center">
          <h3><i class="mdi-content-send brown-text"></i></h3>
          <p class="left-align light" style="text-align: center">Sesungguhnya medan kata bukanlah medan khayal, medan aksi bukanlah medan kata. Dan medan jihad yang benar tak semudah medan jihad yang salah. Banyak yang berkhayal tapi sulit mengingkapkan dengan kata-kata. Banyak yang berkata tapi sedikit yang dapat tegar dalam beramal. Dari yang sedikit, banyak yang dapat bekerja, namun sedikit sekali dari mereka yang sanggup memikul tanggung jawab jihad yang keras dan keras ini</p>
          <h4>"Imam Syahid Hasan Al Banna"</h4>
        </div>
      </div>

    </div>
  </div>


  <div class="parallax-container valign-wrapper">
    <div class="section no-pad-bot">
      <div class="container">
        <div class="row center">
            <h5 class="header col s12 light">Sebaik-baik orang di antara kamu adalah orang yang belajar Al Qur’an dan mengajarkannya
            </h5>
        </div>
      </div>
    </div>
    <div class="parallax"><img src="{{asset('images/background3.png')}}" alt="Unsplashed background img 3"></div>
  </div>

  <footer class="page-footer green">
    <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <h5 class="white-text">About</h5>
          <p class="grey-text text-lighten-4">Ukm Al Banna merupakan sebuah organisasi dakwah yang ada di kampus Politeknik Negeri Lampung yang Concern terhadap nilai-nilai islam dan bersendikan pada Al Qur’an dan As Sunnah</p>
        </div>
        <div class="col l3 s12">
          <h5 class="white-text">Social Media</h5>
          <ul>
            <li><a class="white-text" href="#!">Facebook</a></li>
            <li><a class="white-text" href="#!">Youtube</a></li>
            <li><a class="white-text" href="#!">Instagram</a></li>
            <li><a class="white-text" href="#!">Twitter</a></li>
          </ul>
        </div>
        <div class="col l3 s12">
          <h5 class="white-text">Link</h5>
          <ul>
            <li><a class="white-text" href="#!">Contact</a></li>
            <li><a class="white-text" href="#!">About</a></li>
            <li><a class="white-text" href="#!">History</a></li>
            <li><a class="white-text" href="#!">Disclaimer</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
        <a class="brown-text text-lighten-3" href="https://rafibanget.blogspot.com">BMQ Version 1.0</a>
      </div>
    </div>
  </footer>


  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="{{asset('plugins/materialize-css/js/materialize-v1.js')}}"></script>
  <script type="text/javascript">
    (function($){
      $(function(){

        $('.sidenav').sidenav();
        $('.parallax').parallax();

      }); // end of document ready
    })(jQuery); // end of jQuery name space              
  </script>

  </body>
</html>
