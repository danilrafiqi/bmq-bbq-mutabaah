<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Prodi;
use Illuminate\Support\Facades\Hash;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $users = User::all();

        return view('admin.users.index',compact('users'));         
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $prodis = Prodi::all();
        return view('admin.users.create', compact('prodis'));         
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $file = $request->file('foto');
        $name = 'userbmq' . \Carbon\Carbon::now()->format('YmdHis') . '.' . $file->getClientOriginalExtension();
        $namafoto = $name;
        $foto = $file->storeAs('public/uploads', $name);

        User::create([
            'npm' => $request->get('npm'),
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
            'sex' => $request->get('sex'),
            'id_prodi' => $request->get('id_prodi'),
            'role' => $request->get('role'),
            'foto' => $namafoto,
        ]);
        return redirect('user')->with('success','Berhasil Tambah Data');                
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $user = User::find($id);
        $prodis = Prodi::all();
        return view('admin.users.edit',compact('user','id', 'prodis'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $file = $request->file('foto'); 
        if($file != null){
            $name = 'userbmq' . \Carbon\Carbon::now()->format('YmdHis') . '.' . $file->getClientOriginalExtension();
            $namafoto = $name;
            $foto = $file->storeAs('public/uploads', $name);
        }else{
            $file = User::find($id);            
            $namafoto = $file['foto'];
        }
        $user = User::find($id)
            ->update([
                'npm' => $request->get('npm'),
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                // 'password' => Hash::make($request->get('password')),
                'sex' => $request->get('sex'),
                'id_prodi' => $request->get('id_prodi'),
                'role' => $request->get('role'),
                'foto' => $namafoto,                
            ]);
        return redirect('user')->with('success','Berhasil Tambah Data');        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user = User::find($id);
        $user->delete();
        return redirect('user')->with('success','User Has Been Deleted');                  
    }
}
