<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mutabaah;
use Excel;
use Illuminate\Support\Facades\DB;
use PDF;

class MutabaahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
        //

        $akses = $request->user()->role;
        $npm = $request->user()->npm;

        if($akses == 'admin'){
            $mutabaahs = Mutabaah::when(
                $request->keyword, 
                function ($query) use ($request) {
                    $query->where('npm', 'like', "%{$request->keyword}%");
                })
            ->orderBy('npm')
            ->orderBy('minggu')
            ->latest()
            ->paginate(10);
            $mutabaahs->appends($request->only('keyword'));

            return view('admin.mutabaahs.index',compact('mutabaahs'))->with('i', (request()->input('page', 1) - 1) * 5);
        }else{        
            $mutabaahs = Mutabaah::when(
                $request->keyword, 
                function ($query) use ($request) {
                    $query->where('id', 'like', "%{$request->keyword}%");
                })
            ->where('npm', $npm)
            ->orderBy('minggu')
            ->paginate(10);
            $mutabaahs->appends($request->only('keyword'));
            
            return view('admin.mutabaahs.index',compact('mutabaahs'))->with('i', (request()->input('page', 1) - 1) * 5);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $npm = $request->user()->npm;
        return view('admin.mutabaahs.create', compact('npm'));        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $mutabaah = new Mutabaah();
        $mutabaah->sholat_wajib = $request->get('sholat_wajib');
        $mutabaah->jamaah_awal_waktu = $request->get('jamaah_awal_waktu');
        $mutabaah->subuh_jamaah = $request->get('subuh_jamaah');
        $mutabaah->qiyamul_lail = $request->get('qiyamul_lail');
        $mutabaah->witir = $request->get('witir');
        $mutabaah->istighfar = $request->get('istighfar');
        $mutabaah->puasa_sunah = $request->get('puasa_sunah');
        $mutabaah->tilawah = $request->get('tilawah');
        $mutabaah->matsurat = $request->get('matsurat');
        $mutabaah->duha = $request->get('duha');
        $mutabaah->baca_buku_islam = $request->get('baca_buku_islam');
        $mutabaah->hafalan_quran = $request->get('hafalan_quran');
        $mutabaah->hafalan_hadis = $request->get('hafalan_hadis');
        $mutabaah->minggu = $request->get('minggu');
        $mutabaah->npm = $request->get('npm');
        $mutabaah->save();

        return redirect('mutabaah')->with('success','Berhasil Tambah Data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        //
        $mutabaah = Mutabaah::find($id);
        $akses = $request->user()->role;
        $user_npm = $request->user()->npm;
        $npm_v = $mutabaah->npm;
        if($user_npm == $npm_v or $akses == 'admin'){
            return view('admin.mutabaahs.edit',compact('mutabaah','id'));
        }else{
            return view('indeks');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $mutabaah = Mutabaah::find($id);
        $mutabaah->sholat_wajib = $request->get('sholat_wajib');
        $mutabaah->jamaah_awal_waktu = $request->get('jamaah_awal_waktu');
        $mutabaah->subuh_jamaah = $request->get('subuh_jamaah');
        $mutabaah->qiyamul_lail = $request->get('qiyamul_lail');
        $mutabaah->witir = $request->get('witir');
        $mutabaah->istighfar = $request->get('istighfar');
        $mutabaah->puasa_sunah = $request->get('puasa_sunah');
        $mutabaah->tilawah = $request->get('tilawah');
        $mutabaah->matsurat = $request->get('matsurat');
        $mutabaah->duha = $request->get('duha');
        $mutabaah->baca_buku_islam = $request->get('baca_buku_islam');
        $mutabaah->hafalan_quran = $request->get('hafalan_quran');
        $mutabaah->hafalan_hadis = $request->get('hafalan_hadis');
        $mutabaah->minggu = $request->get('minggu');
        $mutabaah->save();
        return redirect('mutabaah')->with('success','Berhasil Edit Data');        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //
        $mutabaah = Mutabaah::find($id);



        $akses = $request->user()->role;
        $user_npm = $request->user()->npm;
        $npm_v = $mutabaah->npm;
        if($user_npm == $npm_v or $akses == 'admin'){
            $mutabaah->delete();
            return redirect('mutabaah')->with('success','Employee Has Been Deleted');
        }else{
            return view('indeks');
        }        
    }

    //Export Excel
    public function download($type)
    {
        $data = Mutabaah::get()->toArray();
        return Excel::create('laporan', function($excel) use ($data) {
            $excel->sheet('mutabaah', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
        })->download($type);
    }

    //untuk user
    public function pdf(Request $request){
      $npm = $request->user()->npm;
      $tes = DB::getSchemaBuilder()->getColumnListing('mutabaahs');  
      $mahasiswa = DB::table('users')
        ->join('prodis', 'users.id_prodi', '=', 'prodis.id')
        ->select( 'prodis.nama_prodi as namaprodi','users.name as nama', 'users.npm')
        ->where('users.npm', $npm)
        ->get();

      $mutabaahs = Mutabaah::where('npm', $npm)->orderBy('npm', 'asc')->orderBy('minggu', 'asc')->get();
      $pdf = PDF::loadView('admin.mutabaahs.pdf', compact('mutabaahs', 'mahasiswa'));
      return $pdf->stream();
      // return view('admin.mutabaahs.pdf', compact('mutabaahs', 'mahasiswa'));  
    }

    public function grafik(Request $request){
      // $mutabaahs = Mutabaah::all();
      $npm = $request->user()->npm;
      $mutabaahs = Mutabaah::where('npm', $npm)->orderBy('minggu')->get();
      return view('admin.mutabaahs.grafik', compact('mutabaahs'));
    }
}
